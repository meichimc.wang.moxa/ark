# Ark practice

[TOC]



## Overview

Based on EVB 2-cut



## SD

[SD卡介紹Link](https://blog.csdn.net/qq_41650023/article/details/125126013)

[SD disk access Zephyr API functions](https://docs.zephyrproject.org/3.1.0/services/storage/disk/access.html)

SD卡支援3.0規範規格，即SDXC (secure digital extended capacity)，低容量記憶卡仍使用FAT32，最高速為104MB/s。高容量於SD4.0中規範最大容量為2TB，檔案系統規格為exFAT，速度提升至312MB/s，SDXC電子界面與SDHC相容。



SDHC 與 **SD** 的主要差異在於，舊版本使用 **FAT16** 檔案系統，意思是管理檔案所在位置的表格用 **16 位元**表示，所以最多只能管理 65,536 個範圍，再考慮每個範圍能儲存 32KB 的資料量，所以 65536 × 32KB = 2GB，SD 卡**容量上限只能到達 2GB**。為解決 FAT16 格式可支援容量有限的問題，**SDHC** 改用了 **FAT32** 格式；依規格定義，**容量最大可達到 32GB**。



WP: write protect，SD卡上有個缺口，裡面有個塑膠片可以前後撥動，在插入SD卡座後會碰著一個簧片，塑膠片的位置不一樣導致簧片的位置不一樣，那麼WP pin的狀態不一樣（接地或不接），最終CPU可以根據WP的狀態判斷是否“防寫”。

CD: card detect pin



![SD-2](Ark_practice_images/SD-2.jpg)

![](Ark_practice_images/SD-1.jpg) 



Secure Digital Card (SD) is an evolution of the old MMC![](Ark_practice_images/SD-3.jpg)

### SD file system 架構

![](Ark_practice_images/SD架構.png)



### Test item

建立一個file至SD card，讀取file 內容再將讀取的內容+1後寫入file (write_buffer), 再次讀取file內容
(read_buffer)，透過比對write_buffer與read_buffer內容是否相同判斷是否PASS



SD card取出GPIO2 pin28 狀態為high

SD card插入GPIO2 pin28 狀態為low



### Code

**pinmux.c**: 

- 使用SDHC1 module

- 4bit 資料傳輸
- card detect是用gpio實踐，而不是SDHC module功能

```c
#if DT_NODE_HAS_STATUS(DT_NODELABEL(usdhc1), okay) && CONFIG_DISK_DRIVER_SDMMC

/*Drive Strength Field: R0(260 Ohm @ 3.3V, 150 Ohm@1.8V, 240 Ohm for DDR)
 *Speed Field: medium(100MHz)
 *Open Drain Enable Field: Open Drain Disabled
 *Pull / Keep Enable Field: Pull/Keeper Enabled
 *Pull / Keep Select Field: Pull
 *Pull Up / Down Config. Field: 47K Ohm Pull Up
 *Hyst. Enable Field: Hysteresis Enabled.
 */

static void mimxrt1060_evk_usdhc_pinmux(uint16_t nusdhc, bool init, uint32_t speed,
					uint32_t strength)
{
	uint32_t cmd_data = IOMUXC_SW_PAD_CTL_PAD_SPEED(speed) |
			 IOMUXC_SW_PAD_CTL_PAD_SRE_MASK |
			 IOMUXC_SW_PAD_CTL_PAD_PKE_MASK |
			 IOMUXC_SW_PAD_CTL_PAD_PUE_MASK |
			 IOMUXC_SW_PAD_CTL_PAD_HYS_MASK |
			 IOMUXC_SW_PAD_CTL_PAD_PUS(1) |
			 IOMUXC_SW_PAD_CTL_PAD_DSE(strength);

	uint32_t clk = IOMUXC_SW_PAD_CTL_PAD_SPEED(speed) |
		    IOMUXC_SW_PAD_CTL_PAD_SRE_MASK |
		    IOMUXC_SW_PAD_CTL_PAD_PKE_MASK |
		    // IOMUXC_SW_PAD_CTL_PAD_HYS_MASK |
		    IOMUXC_SW_PAD_CTL_PAD_PUS(0) |
		    IOMUXC_SW_PAD_CTL_PAD_DSE(strength);

	if (nusdhc != 0) {
		LOG_ERR("Invalid USDHC index");
		return;
	}

	if (init) {
		// IOMUXC_SetPinMux(IOMUXC_GPIO_AD_B0_05_GPIO1_IO05, 0U);

		/* SD_CD */
		IOMUXC_SetPinMux(IOMUXC_GPIO_B1_12_GPIO2_IO28, 0U);
		// IOMUXC_SetPinMux(IOMUXC_GPIO_B1_14_USDHC1_VSELECT, 0U);
		IOMUXC_SetPinMux(IOMUXC_GPIO_SD_B0_00_USDHC1_CMD, 0U);
		IOMUXC_SetPinMux(IOMUXC_GPIO_SD_B0_01_USDHC1_CLK, 0U);
		IOMUXC_SetPinMux(IOMUXC_GPIO_SD_B0_02_USDHC1_DATA0, 0U);
		IOMUXC_SetPinMux(IOMUXC_GPIO_SD_B0_03_USDHC1_DATA1, 0U);
		IOMUXC_SetPinMux(IOMUXC_GPIO_SD_B0_04_USDHC1_DATA2, 0U);
		IOMUXC_SetPinMux(IOMUXC_GPIO_SD_B0_05_USDHC1_DATA3, 0U);

		// IOMUXC_SetPinConfig(IOMUXC_GPIO_AD_B0_05_GPIO1_IO05, 0x10B0u);

		/* SD0_CD_SW */
		IOMUXC_SetPinConfig(IOMUXC_GPIO_B1_12_GPIO2_IO28, 0x017089u);

		/* SD0_VSELECT */
		// IOMUXC_SetPinConfig(IOMUXC_GPIO_B1_14_USDHC1_VSELECT, 0x0170A1u);
	}
	IOMUXC_SetPinConfig(IOMUXC_GPIO_SD_B0_00_USDHC1_CMD, cmd_data);
	IOMUXC_SetPinConfig(IOMUXC_GPIO_SD_B0_01_USDHC1_CLK, clk);
	IOMUXC_SetPinConfig(IOMUXC_GPIO_SD_B0_02_USDHC1_DATA0, cmd_data);
	IOMUXC_SetPinConfig(IOMUXC_GPIO_SD_B0_03_USDHC1_DATA1, cmd_data);
	IOMUXC_SetPinConfig(IOMUXC_GPIO_SD_B0_04_USDHC1_DATA2, cmd_data);
	IOMUXC_SetPinConfig(IOMUXC_GPIO_SD_B0_05_USDHC1_DATA3, cmd_data);
}
#endif
```

**sd.c**

```
/**
 *
 * \copyright
 * Copyright (C) MOXA Inc. All rights reserved.
 * This software is distributed under the terms of the
 * MOXA License.  See the file COPYING-MOXA for details.
 *
 */

/* Sample which uses the filesystem API and SDHC driver */
#include <logging/log.h>
LOG_MODULE_REGISTER(sd, LOG_LEVEL_INF);

#include <zephyr.h>
#include <stdio.h>
#include <device.h>
#include <storage/disk_access.h>
#include <fs/fs.h>
#include <ff.h>
#include <shell/shell.h>

#include "bl_common.h"
// #include "env.h"

#include <fsl_gpio.h>

#define MAX_PATH_LEN 255

static FATFS fat_fs;
/* mounting info */
static struct fs_mount_t mp = {
	.type = FS_FATFS,
	.fs_data = &fat_fs,
};

/*
*  Note the fatfs library is able to mount only strings inside _VOLUME_STRS
*  in ffconf.h
*/
static const char *disk_mount_pt = "/SD:";

static int cmd_sd(const struct shell *shell, size_t argc, char **argv)
{
	/* raw disk i/o */
	do {
		static const char *disk_pdrv = "SD";
		uint64_t memory_size_mb;
		uint32_t block_count;
		uint32_t block_size;
		LOG_DBG("SD test !\n"); // meichi test

		printk("CD status: %d\n", GPIO_PinRead(GPIO2,28));

		uint32_t cd_cnt = 0; // SD not instered
		for (int i=0; i<10; i++)
		{
			if (GPIO_PinRead(GPIO2,28) == 0)
			{
				cd_cnt++;
			}
			k_sleep(K_MSEC(500)); //delay 500ms
		}
		if (cd_cnt < 6)
		{
			printk("Please insert SD card\n");
			printk("CD current status: %d\n", GPIO_PinRead(GPIO2,28));
			return 0;
		}
		else
		{
			printk("Please is inserted SD card\n");
			printk("CD current status: %d\n", GPIO_PinRead(GPIO2,28));
		}

		if (disk_access_init(disk_pdrv) != 0) {
			LOG_ERR("Storage init ERROR!");
			break;
		}

		if (disk_access_ioctl(disk_pdrv,
				DISK_IOCTL_GET_SECTOR_COUNT, &block_count)) {
			LOG_ERR("Unable to get sector count");
			break;
		}
		LOG_INF("Block count %u", block_count);

		if (disk_access_ioctl(disk_pdrv,
				DISK_IOCTL_GET_SECTOR_SIZE, &block_size)) {
			LOG_ERR("Unable to get sector size");
			break;
		}
		printk("Sector size %u\n", block_size);

		memory_size_mb = (uint64_t)block_count * block_size;
		printk("Memory Size(MB) %u\n", (uint32_t)(memory_size_mb >> 20));
	} while (0);

	return test_sd_fat_read_write();
}
SHELL_CMD_ARG_REGISTER(sd, NULL, "Run SD test", cmd_sd, 1, 0);

int test_sd_fat_read_write()
{
	char fname[MAX_PATH_LEN]; //255bytes
	struct fs_statvfs sbuf;
	
	//f_bsize	Optimal transfer block size
	//f_frsize	Allocation unit size
	//f_blocks	Size of FS in f_frsize units
	//f_bfree	Number of free blocks

	int rc;

	mp.mnt_point = disk_mount_pt; //Mount point directory name (ex: "/SD:")

	rc = fs_mount(&mp);
	if (rc != FR_OK) {
		LOG_ERR("Disk mount failed.\n");
		return -1;
	}

	snprintf(fname, sizeof(fname), "%s/testcnt", mp.mnt_point); // fname = /SD:/testcnt

	rc = fs_statvfs(mp.mnt_point, &sbuf);
	if (rc < 0) {
		LOG_ERR("FAIL: statvfs: %d\n", rc);
		goto ERR_EXIT;
	}

	LOG_DBG("%s: bsize = %lu ; frsize = %lu ;"
	       " blocks = %lu ; bfree = %lu\n",
	       mp.mnt_point,
	       sbuf.f_bsize, sbuf.f_frsize,
	       sbuf.f_blocks, sbuf.f_bfree);

	struct fs_dirent dirent;

	rc = fs_stat(fname, &dirent);
	LOG_DBG("%s stat: %d\n", fname, rc);
	if (rc >= 0) {
		LOG_DBG("\tfn '%s' size %u\n", dirent.name, dirent.size);
	}

	struct fs_file_t file;

	uint32_t test_count = 0;
	uint32_t read_value = 0;

	fs_file_t_init(&file);

	rc = fs_open(&file, fname, FS_O_CREATE | FS_O_RDWR);
	if (rc < 0) {
		LOG_ERR("FAIL: open %s: %d\n", fname, rc);
		goto ERR_EXIT;
	}

	if (rc >= 0) { //success open the file
		rc = fs_read(&file, &test_count, sizeof(test_count));
		LOG_DBG("%s read count %u: %d\n", fname, test_count, rc);
		printk("%s read count %u: %d\n", fname, test_count, rc); //meichi test
		rc = fs_seek(&file, 0, FS_SEEK_SET);
		LOG_DBG("%s seek start: %d\n", fname, rc);
	}

	test_count += 1;

	rc = fs_write(&file, &test_count, sizeof(test_count));
	LOG_DBG("%s write new boot count %u: %d\n", fname, test_count, rc);

	rc = fs_sync(&file);
	LOG_DBG("%s sync %d\n", fname, rc);

	rc = fs_seek(&file, 0, FS_SEEK_SET);
	LOG_DBG("%s seek start: %d\n", fname, rc);

	rc = fs_read(&file, &read_value, sizeof(read_value));
	LOG_DBG("%s read value %u: %d\n", fname, read_value, rc);
	printk("%s read back value %u: %d\n", fname, read_value, rc); //meichi test

	rc = fs_close(&file);
	LOG_DBG("%s close: %d\n", fname, rc);

	if (read_value != test_count) {
		LOG_ERR("Test failed.\n");
		goto ERR_EXIT;
	}

	rc = fs_unmount(&mp);
	LOG_DBG("%s unmount: %d\n", mp.mnt_point, rc);
	printk("SD Test PASS\n");
	return 0;

ERR_EXIT:
	rc = fs_unmount(&mp);
	LOG_DBG("%s unmount: %d\n", mp.mnt_point, rc);
	return -1;
}
```

Output:

```
CD status: 0
Please is inserted SD card
CD current status: 0
[00:00:08.871,000] <inf> usdhc: SD inserted!
[00:00:08.999,000] <inf> sd: Block count 62333952
Sector size 512
Memory Size(MB) 30436
/SD:/testcnt read count 3276550: 4
/SD:/testcnt read back value 3276551: 4
SD Test PASS

```



### Appendix

snprintf function test:

```
#include <stdio.h>

int main()
{
    char fname[255];
    snprintf(fname, sizeof(fname), "%s/testcnt", "/SD:");
    printf("%s", fname);
    return 0;
}
```

Output:

```
/SD:/testcnt
```



## AI

### Test item

ADS1261 register 設定與讀取功能



### Code

**Y:\zephyr_org\bios-moxasrc\moxa\boards\arm\moxa_evb\pinmux.c**

```
	//add ADC1261 sel pin
	IOMUXC_SetPinMux(IOMUXC_GPIO_AD_B0_03_GPIO1_IO03, 0);
	IOMUXC_SetPinConfig(IOMUXC_GPIO_AD_B0_03_GPIO1_IO03,
			    IOMUXC_SW_PAD_CTL_PAD_PKE_MASK |
				IOMUXC_SW_PAD_CTL_PAD_PUS_MASK |
				IOMUXC_SW_PAD_CTL_PAD_PUE_MASK |
			    IOMUXC_SW_PAD_CTL_PAD_SPEED(2) |
			    IOMUXC_SW_PAD_CTL_PAD_DSE(6));

	GPIO_PinInit(GPIO1, 3, &gpio_config_output);
	GPIO_WritePinOutput(GPIO1, 3, 1); // 1
```



**Y:\zephyr_org\bios-moxasrc\moxa\boards\arm\moxa_evb\moxa_evb.dts**

```
aliases {
		led0 = &led_0;
		wdi0 = &wdi_0;
		pwm-led0 = &green_pwm_led;
		green-pwm-led = &green_pwm_led;
		jp1 = &jp_1;
		//cd-gpios = &cd_gpios;
		//meichi test ads1261 CS#
		cs0 = &cs_0;
		pwdn0 = &pwdn_0;
		lpspi3-drdy = &lpspi3_drdy_int;
		//switch = &switch_int;
	};
	
leds {
		compatible = "gpio-leds";
		led_0: led-0 {
			gpios = <&gpio1 0 GPIO_ACTIVE_LOW>;
			label = "LED 0";
		};

		wdi_0: led-1 {
			gpios = <&gpio3 3 GPIO_ACTIVE_LOW>;
			label = "WDI";
		};
		
		cs_0: led-2 {
			gpios = <&gpio1 3 GPIO_ACTIVE_HIGH>;
			label = "CS";
		}; 

		pwdn_0: led-3 {
			gpios = <&gpio2 8 GPIO_ACTIVE_LOW>;
			label = "PWDN";
		};
	};
	
&cs_0 {
	status = "okay";
};
```



**Y:\zephyr_org\bios-moxasrc\modules\board_lib\src\gpio.c**

```
int init_gpio(void)
{
	init_jp1();
	init_led0();
	init_wdi0();
	//init_lpspi3_drdy(); //meichi test
	init_cs0(); //meichi test
	//init_pwdn0(); //meichi test
	//switch_int_init();
#if defined (CONFIG_BOARD_LIB_MXGPIO_THREAD)
	init_gpio_thread();
#endif
	led0_on(true);
	_cs0_high(false); //meichi test
	return 0;
}

/*
 *
 * CS# (chip select low active) meichi test
 *
 */
#define CS0_NODE		DT_ALIAS(cs0)
#define CS0_GPIO_LABEL	DT_GPIO_LABEL(CS0_NODE, gpios)
#define CS0_GPIO_PIN	DT_GPIO_PIN(CS0_NODE, gpios)
#define CS0_GPIO_FLAGS	(GPIO_OUTPUT | DT_GPIO_FLAGS(CS0_NODE, gpios))
const struct device *cs0 = NULL;
const struct device *init_cs0(void)
{
	int ret;

	cs0 = device_get_binding(CS0_GPIO_LABEL);
	if (cs0 == NULL) {
		printk("Error: didn't find %s device\n", CS0_GPIO_LABEL);
		return NULL;
	}

	ret = gpio_pin_configure(cs0, CS0_GPIO_PIN, CS0_GPIO_FLAGS);
	if (ret != 0) {
		printk("Error %d: failed to configure %s pin %d\n", ret, CS0_GPIO_LABEL, CS0_GPIO_PIN);
		return NULL;
	}

	return cs0;
}

void _cs0_high(bool val)
{
	if (cs0) {
		gpio_pin_set(cs0, CS0_GPIO_PIN, val);
	}
}

int _cs0_get(void)
{
	return GPIO_PinRead(cs0, 3);
}
```



**Y:\zephyr_org\bios-moxasrc\profile\mpos\src\ai_meichi_dev.c**

```
#include <init.h>
#include <drivers/gpio.h>
#include <fsl_iomuxc.h>
#include <fsl_gpio.h>
#include <fsl_semc.h>
#include "fsl_lpspi.h"
#include <soc.h>
#include <logging/log.h>
#include <stdio.h>
#include <shell/shell.h>
#include <zephyr.h>

#include <ADS1261.h>

extern uint8_t unlock_rx[4];
extern uint8_t unlock_tx[4];
extern uint8_t reg_tx[6];
extern uint8_t reg_rx[6];

static int cmd_ads1261_init(const struct shell *shell, size_t argc, char *argv[])
{
    uint8_t ADC_Index = 0;
    uint8_t unlocked = 0;
    if(argc < 2) {
		return 0;
	}

    ADC_Index = (uint8_t)strtoul(argv[1], NULL, 10);

    ADC_RegisterMap[REG_ADDR_ID] 			= 	ID_DEV_ADS1261 & ID_REV_A;
	ADC_RegisterMap[REG_ADDR_STATUS] 		= 	STATUS_DEFAULT;
	ADC_RegisterMap[REG_ADDR_MODE0]			= 	MODE0_DEFAULT;
	ADC_RegisterMap[REG_ADDR_MODE1] 		= 	MODE1_DEFAULT;
    /*  mode2
        set AIN4 as GPIO2
        set AIN3 as GPIO1
        set AIN2 as GPIO0 
    */
	ADC_RegisterMap[REG_ADDR_MODE2] 		= 	MODE2_DEFAULT;
	ADC_RegisterMap[REG_ADDR_MODE3] 		= 	MODE3_DEFAULT;
    /*  REF voltage
        enable internal reference voltage = 2.5V 
    */
	ADC_RegisterMap[REG_ADDR_REF] 			= 	REF_DEFAULT;
	ADC_RegisterMap[REG_ADDR_OFCAL0] 		= 	OFCAL0_DEFAULT;
	ADC_RegisterMap[REG_ADDR_OFCAL1] 		= 	OFCAL1_DEFAULT;
	ADC_RegisterMap[REG_ADDR_OFCAL2] 		= 	OFCAL2_DEFAULT;
	ADC_RegisterMap[REG_ADDR_FSCAL0] 		= 	FSCAL0_DEFAULT;
	ADC_RegisterMap[REG_ADDR_FSCAL1] 		= 	FSCAL1_DEFAULT;
	ADC_RegisterMap[REG_ADDR_FSCAL2] 		= 	FSCAL2_DEFAULT;
    /*  IMUX
        IDAC2 analog input pin connection = no connection
        IDAC1 analog input pin connection = no connection
    */
	ADC_RegisterMap[REG_ADDR_IMUX] 			= 	IMUX_DEFAULT;
	ADC_RegisterMap[REG_ADDR_IMAG] 			= 	IMAG_DEFAULT;
	ADC_RegisterMap[REG_ADDR_PGA] 			= 	PGA_DEFAULT;
    /*  INPMUX
        Positive AIN8 (ADS1261 only)
        Negative AIN9 (ADS1261 only)
    */
	ADC_RegisterMap[REG_ADDR_INPMUX]		= 	INPMUX_DEFAULT;
	ADC_RegisterMap[REG_ADDR_INPBIAS]		= 	INPBIAS_DEFAULT;

    //CS low active
    shell_print(shell, "get CS pin status = %d\n",  _cs0_get());

    unlocked = unlockRegisters(ADC_Index);
    //unlock_rx = unlockRegisters(ADC_Index); //meichi test
    shell_print(shell,"Unlock tramsmitted data: \n");
    for (int i=0; i<4; i++) {
        shell_print(shell,"%d \n", unlock_tx[i]);
    }
    shell_print(shell,"Unlock reveived data: \n");
    for (int i=0; i<4; i++) {
        shell_print(shell,"%d \n", unlock_rx[i]);
    }

    if (unlocked) {
        shell_print(shell,"ADC register is locked!\n");
    } else {
        shell_print(shell,"ADC register is unlocked successfully!\n");
    }


	writeSingleRegister(ADC_Index, REG_ADDR_MODE0,MODE0_DEFAULT);
	k_usleep(10);
	writeSingleRegister(ADC_Index, REG_ADDR_MODE1,MODE1_DEFAULT);
	k_usleep(10);
	writeSingleRegister(ADC_Index, REG_ADDR_MODE2,MODE2_DEFAULT);
	k_usleep(10);
	writeSingleRegister(ADC_Index, REG_ADDR_MODE3,MODE3_DEFAULT);
	k_usleep(10);
	writeSingleRegister(ADC_Index, REG_ADDR_REF,REF_DEFAULT);
	k_usleep(10);
	writeSingleRegister(ADC_Index, REG_ADDR_PGA,PGA_DEFAULT);
	k_usleep(10);
	writeSingleRegister(ADC_Index, REG_ADDR_INPMUX,INPMUX_DEFAULT);

	k_usleep(5000);

    return 0;
}

#define AIREG_SET_CMD_HELP 	"AI REG SET operation\n"\
							"ex: ai_reg_set ADC_Index [reg] [reg_data]\n"\
							"[reg]: 0 - 18(0x00-0x12)\n"\
                            "[reg_data]: 0-255 (0x00 - 0xFF)"
static int cmd_ads1261_reg_set(const struct shell *shell, size_t argc, char *argv[])
{
    // ads1261 set addr reg_data
    uint8_t ADC_Index = 0;
    uint32_t Addr = 0;
    uint32_t RegData = 0;

    if(argc < 4) {
        shell_print(shell, AIREG_SET_CMD_HELP);
		return 0;
	}
    else {
        ADC_Index = (uint8_t)strtoul(argv[1], NULL, 10);
        Addr = (uint8_t)strtoul(argv[2], NULL, 16);
        RegData = (uint8_t)strtoul(argv[2], NULL, 16);
    }

    ADC_RegisterMap[Addr] = RegData;

    shell_print(shell, "get CS pin status = %d\n",  _cs0_get());

    unlockRegisters(ADC_Index);
    writeSingleRegister(ADC_Index, Addr, RegData);
    k_usleep(5000);
	shell_print(shell, "ADS1261 spi[%d] set ok", ADC_Index);

    return 0;
}


#define AIREG_GET_CMD_HELP 	"AI REG GET operation\n"\
							"ex: ai_reg_get [reg]\n"\
							"[reg]: 0 - 18(0x00-0x12)\n"
static int cmd_ads1261_reg_get(const struct shell *shell, size_t argc, char *argv[])
{
    uint8_t counter = 0;
    uint8_t ADC_Index = 0;
    uint32_t Addr=0;

    if(argc < 3) {
        shell_print(shell, AIREG_GET_CMD_HELP);
		return 0;
	} else {
        ADC_Index = (uint8_t)strtoul(argv[1], NULL, 10);
        Addr = (uint8_t)strtoul(argv[2], NULL, 16);
    }
    //CS low active
    shell_print(shell, "get CS pin status = %d\n",  _cs0_get());
    shell_print(shell, "ADC_spi[%d]_REG[%d] : 0x%02x", ADC_Index, Addr,readSingleRegister(ADC_Index, Addr));

    shell_print(shell,"Read_Reg tramsmitted data: \n");
    for (int i=0; i<6; i++) {
        shell_print(shell,"%d \n", reg_tx[i]);
    } 

    shell_print(shell,"Read_Reg reveived data: \n");
    for (int i=0; i<6; i++) {
       shell_print(shell,"%d \n", reg_rx[i]);
    }

    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(sub_ads1261,
	SHELL_CMD(init, NULL, "ex: ads1261 init", cmd_ads1261_init),
    SHELL_CMD(reg_set, NULL, "ex: ads1261 reg_set [reg]:(0-18)[value]:(0-255)", cmd_ads1261_reg_set),
	SHELL_CMD(reg_get, NULL, "ex: ads1261 reg_get [reg]:(0-18)", cmd_ads1261_reg_get),
	SHELL_SUBCMD_SET_END
);

SHELL_CMD_REGISTER(ads1261, &sub_ads1261, "ADS1261 init/set/get value", NULL);

```



### Note (device tree active status)

device tree 設定，active 時輸出為高電位 

```
cs_0: led-2 {
			gpios = <&gpio1 3 GPIO_ACTIVE_HIGH>;
			label = "CS";
		}; 
```

`gpio_pin_set(cs0, CS0_GPIO_PIN, true);` 高電位

`gpio_pin_set(cs0, CS0_GPIO_PIN, false);` 低電位



build不過的問題已排除

於modules\hal\nxp\mcux\drivers\imx\CMakeLists.txt加入ads1261 function config

```
#meichi test
zephyr_sources(fsl_lpspi.c)
```



## UART

serial port object = tty

RS232, 422, 485, 485r loopback 測試

RS232 RTS和CTS轉換為GPIO pin後互接測試，refer to `static int cts_rts_lp_test_cmd(const struct shell *shell, size_t argc, char **argv)`



### Code

**Y:\zephyr_org\bios-moxasrc\modules\board_lib\src\uart.c** (moxa driver)

```c
static struct tty_serial uart2_moxa_serial;
static struct tty_serial uart3_moxa_serial;
static struct tty_serial uart4_moxa_serial;

static uint8_t uart2_rxbuf[CONFIG_UART_RX_BUFSIZE];
static uint8_t uart2_txbuf[CONFIG_UART_TX_BUFSIZE];
static uint8_t uart3_rxbuf[CONFIG_UART_RX_BUFSIZE];
static uint8_t uart3_txbuf[CONFIG_UART_TX_BUFSIZE];
static uint8_t uart4_rxbuf[CONFIG_UART_RX_BUFSIZE];
static uint8_t uart4_txbuf[CONFIG_UART_TX_BUFSIZE];

int uart_dev_init(const char *name, 
	struct tty_serial *serial_dev, 
	uint8_t *tx_buf, size_t tx_size, 
	uint8_t *rx_buf, size_t rx_size)
{
	const struct device *uart_dev;
	int ret = 0;

	if (name == NULL || serial_dev == NULL || 
		tx_buf == NULL || rx_buf == NULL) {
		return -1;
	}
	// name: "UART_2", "UART_3", "UART_4"
	uart_dev = device_get_binding(name);
    // serial_dev: uart2_moxa_serial, uart3_moxa_serial, uart4_moxa_serial
    // return 0, if success
	ret = tty_init(serial_dev, uart_dev);

	if (ret) {
		return ret;
	}

	/* Checks device driver supports for interrupt driven data transfers. */
	if (CONFIG_UART_RX_BUFSIZE + CONFIG_UART_TX_BUFSIZE) {
		const struct uart_driver_api *api =
			(const struct uart_driver_api *)uart_dev->api;
		if (!api->irq_callback_set) {
			return -ENOTSUP;
		}
	}

	tty_set_tx_timeout(serial_dev, 100); //100 milliseconds
	tty_set_rx_timeout(serial_dev, 100);
	tty_set_tx_buf(serial_dev, tx_buf, tx_size);
	tty_set_rx_buf(serial_dev, rx_buf, rx_size);

	return 0;
}
```



**Y:\zephyr_org\bios-moxasrc\modules\board_lib\include\board_lib.h** (uart.c沒有header file，functions都被放在board_lib.h中)

```c
/* UART */
#define CONFIG_UART_RX_BUFSIZE 1024
#define CONFIG_UART_TX_BUFSIZE 1024
void rs232_config_init(void);
void rs422_config_init(void);
void rs485_config_init_P1Tx_P2Rx(void);
void rs485_config_init_P1Rx_P2Tx(void);
int uart_init(void);
ssize_t uart_write_b2b(const void *buf, size_t size);
ssize_t uart_read_b2b(void *buf, size_t size);
ssize_t uart_write_p1(const void *buf, size_t size);
ssize_t uart_read_p1(void *buf, size_t size);
ssize_t uart_write_p2(const void *buf, size_t size);
ssize_t uart_read_p2(void *buf, size_t size);
```



**Y:\zephyr_org\bios-moxasrc\profile\mpos\src\sample_func.c**

RS232, 422, 485, 485r loopback 測試

```c
void uart_pin_setting(int mode)
{
	int l232_1_en;
	int l485_p1_en0;
	int l485_p1_en1;
	int l485_p1_en2;
	int l485_p2_en0;
	int l485_p2_en1;

	if(mode == 2) // 422 (UART3)
	{
		GPIO_WritePinOutput(GPIO1,11,0);	//L232_P1_EN
		GPIO_WritePinOutput(GPIO1,8,1);		//l485_P1_EN0
		GPIO_WritePinOutput(GPIO1,9,1);		//l485_P1_EN1
		GPIO_WritePinOutput(GPIO1,10,0);	//l485_P1_EN2
		GPIO_WritePinOutput(GPIO1,14,0);	//l485_P2_EN0
		GPIO_WritePinOutput(GPIO1,15,0);	//l485_P2_EN1
	}
	else if (mode ==3) // 485 (TX:urat3/DATA2 RX:uart4/DATA1)
	{
		GPIO_WritePinOutput(GPIO1,11,0);
		GPIO_WritePinOutput(GPIO1,8,1);
		GPIO_WritePinOutput(GPIO1,9,1);
		GPIO_WritePinOutput(GPIO1,10,0);	//l485_P1_EN2
		GPIO_WritePinOutput(GPIO1,14,1);	//l485_P2_EN0 switch to UART4 Rx
		GPIO_WritePinOutput(GPIO1,15,0);	//l485_P2_EN1
	}
	
	else if (mode ==4) // 485r (TX:urat4/DATA1 RX:uart3/DATA2)
	{
		GPIO_WritePinOutput(GPIO1,11,0);
		GPIO_WritePinOutput(GPIO1,8,0);
		GPIO_WritePinOutput(GPIO1,9,0);
		GPIO_WritePinOutput(GPIO1,10,1);
		GPIO_WritePinOutput(GPIO1,14,1);
		GPIO_WritePinOutput(GPIO1,15,1);
	}

	
	else // 232 (UART3)
	{
		GPIO_WritePinOutput(GPIO1,11,1);
		GPIO_WritePinOutput(GPIO1,8,1);
		GPIO_WritePinOutput(GPIO1,9,0);
		GPIO_WritePinOutput(GPIO1,10,0);
		GPIO_WritePinOutput(GPIO1,14,0);
		GPIO_WritePinOutput(GPIO1,15,1);
	}
	
	l232_1_en = GPIO_PinRead(GPIO1,11);
	l485_p1_en0 = GPIO_PinRead(GPIO1,8);
	l485_p1_en1 = GPIO_PinRead(GPIO1,9);
	l485_p1_en2 = GPIO_PinRead(GPIO1,10);
	l485_p2_en0 = GPIO_PinRead(GPIO1,14);
	l485_p2_en1 = GPIO_PinRead(GPIO1,15);


	printk("l232_1_en = %d\n",l232_1_en);
	printk("l485_p1_en0 = %d\n",l485_p1_en0);
	printk("l485_p1_en1 = %d\n",l485_p1_en1);
	printk("l485_p1_en2 = %d\n",l485_p1_en2);
	printk("l485_p2_en0 = %d\n",l485_p2_en0);
	printk("l485_p2_en1 = %d\n",l485_p2_en1);

}

int do_uart_test_t(int mode)
{
	uint8_t uart_tx_buff[100];
	uint8_t uart_rx_buff[100];


	int i = 0;

	memset(uart_tx_buff, 0, sizeof(uart_tx_buff));
	memset(uart_rx_buff, 0, sizeof(uart_rx_buff));
	//printk("uart_tx_buff =");
	for (i = 0; i < sizeof(uart_tx_buff); i++) {
		uart_tx_buff[i] = (uint8_t)i;
		//printk("%c",uart_tx_buff[i]);

	}

	if(mode == 2) // 422 (UART3)
	{
		uart_write_p1(uart_tx_buff, sizeof(uart_tx_buff));
		uart_read_p1(uart_rx_buff, sizeof(uart_rx_buff));
	}
	else if (mode ==3) // 485 (TX:urat3/DATA2 RX:uart4/DATA1)
	{
		uart_write_p1(uart_tx_buff, sizeof(uart_tx_buff));
		uart_read_p2(uart_rx_buff, sizeof(uart_rx_buff));
	}
	else if (mode ==4) // 485r (TX:urat4 RX:uart3)
	{
		uart_write_p2(uart_tx_buff, sizeof(uart_tx_buff));
		uart_read_p1(uart_rx_buff, sizeof(uart_rx_buff));
	}
	else // 232 (UART3)
	{
		uart_write_p1(uart_tx_buff, sizeof(uart_tx_buff));
		uart_read_p1(uart_rx_buff, sizeof(uart_rx_buff));
	}
	

	printk("uart_rx_buff[50] = %c\n",uart_rx_buff[50]);
	printk("uart_rx_buff[51] = %c\n",uart_rx_buff[51]);
	printk("uart_rx_buff[52] = %c\n",uart_rx_buff[52]);
	
	printk("uart_tx_buff[50] = %c\n",uart_tx_buff[50]);
	printk("uart_tx_buff[51] = %c\n",uart_tx_buff[51]);
	printk("uart_tx_buff[52] = %c\n",uart_tx_buff[52]);

	
	if (memcmp(uart_tx_buff, uart_rx_buff, sizeof(uart_tx_buff)) != 0) {
		printk("Data does not match.");
		return -1;
	}

	return 0;
}

int uart_test(int mode)
{
	int ret = 0;

	uint8_t uart_rx_buff[100];

	uart_pin_setting(mode);
	
	ret = uart_init();
	
	if(mode == 3)
		uart_read_p2(uart_rx_buff, sizeof(uart_rx_buff));
	else
		uart_read_p1(uart_rx_buff, sizeof(uart_rx_buff));
	if (ret)
	{
		printk("uart_test_init: FAIL\n");
		return ret;
	}

	ret = do_uart_test_t(mode);
	if (ret)
	{
		printk("uart_test: FAIL\n");
		return ret;
	}

	printk("uart_test: PASS\n");
	return 0;
}

#define UART_CMD_HELP "uart test.\n"    \
			"Usage: sample_uart_test [1/2/3/4] 1:rs232 2:rs422 3:rs485 4:rs485r "
static int uart_test_cmd(const struct shell *shell, size_t argc, char **argv)
{

	if (argc < 2)
        {
                shell_print(shell,"input too few\n");
                return 0;
        }

	if(strcmp(argv[1], "1") == 0)
	{	
		shell_print(shell,"RS232 TEST\n");
		uart_test(1);
	}
	else if (strcmp(argv[1], "2") == 0)
	{	
		shell_print(shell,"RS422 TEST\n");
		uart_test(2);
	}

	else if (strcmp(argv[1], "3") == 0)
	{	
		shell_print(shell,"RS485 TEST\n");
		uart_test(3);
	}

	else if (strcmp(argv[1], "4") == 0)
	{	
		shell_print(shell,"RS485r TEST\n");
		uart_test(4);
	}

	else
	{	
		shell_print(shell,"RS232 TEST\n");
		uart_test(1);
	}

	return 0;

}

SHELL_CMD_ARG_REGISTER(sample_uart_test, NULL, UART_CMD_HELP, uart_test_cmd,1,1);
```



### Note

```
sleep(2); //delay 2sec
```



uart irq callback function 在 tty init時會assign

```c
int tty_init(struct tty_serial *tty, const struct device *uart_dev)
{
	if (!uart_dev) {
		return -ENODEV;
	}

	tty->uart_dev = uart_dev;

	/* We start in unbuffer mode. */
	tty->rx_ringbuf = NULL;
	tty->rx_ringbuf_sz = 0U;
	tty->tx_ringbuf = NULL;
	tty->tx_ringbuf_sz = 0U;

	tty->rx_get = tty->rx_put = tty->tx_get = tty->tx_put = 0U;

	tty->rx_timeout = SYS_FOREVER_MS;
	tty->tx_timeout = SYS_FOREVER_MS;

	uart_irq_callback_user_data_set(uart_dev, tty_uart_isr, tty); //here

	return 0;
}
```

```c
// UART IRQ 中斷方式處裡data
static void tty_uart_isr(const struct device *dev, void *user_data)
{
	struct tty_serial *tty = user_data; // 裡面會記載rx和tx buffer address與 timeout時間等

	uart_irq_update(dev);

	if (uart_irq_rx_ready(dev)) {
		uint8_t c;

		while (1) {
			if (uart_fifo_read(dev, &c, 1) == 0) { // chech有無data
				break;
			}
			tty_irq_input_hook(tty, c); // 此function會將fifo中的data copy到使用者的rx buffer
		}
	}

	if (uart_irq_tx_ready(dev)) {
		if (tty->tx_get == tty->tx_put) {
			/* Output buffer empty, don't bother
			 * us with tx interrupts
			 */
			uart_irq_tx_disable(dev);
		} else {
			uart_fifo_fill(dev, &tty->tx_ringbuf[tty->tx_get++], 1);
			if (tty->tx_get >= tty->tx_ringbuf_sz) {
				tty->tx_get = 0U;
			}
			k_sem_give(&tty->tx_sem);
		}
	}
}
```





## GPIO

### Test item

以shell介面下command，控制led0 ON/OFF



### Code

**Y:\zephyr\bios-moxasrc\modules\board_lib\src\gpio.c**

```c
// meichi test
// led0 init and get status
#if defined (CONFIG_SHELL) && (CONFIG_LED0_INIT_MEICHI_DEV)
#define LED0_INIT_MEICHI_DEV_CMD_HELP "Moxa LED0 init operation.\n"\
			"Usage: led0_init_meichi_dev\n"

static int cmd_led0_init_meichi_dev(const struct shell *shell, size_t argc, char **argv)
{
	int ret;
	if (led0 == NULL) {
		led0 = device_get_binding(LED0_GPIO_LABEL);

		if (led0 == NULL) {
			printk("Error: didn't find %s device\n", LED0_GPIO_LABEL);
			return NULL;
		} else {
			printk("Success: find %s device\n", LED0_GPIO_LABEL);
		}

		ret = gpio_pin_configure(led0, LED0_GPIO_PIN, LED0_GPIO_FLAGS);
		if (ret != 0) {
			printk("Error: feiled to configure %s pin %d\n", LED0_GPIO_LABEL, LED0_GPIO_PIN);
			return NULL;
		} else {
			printk("Success to configure %s pin %d\n", LED0_GPIO_LABEL, LED0_GPIO_PIN);
		}
	} else {
		printk("Skip initialization and Success to configure %s pin %d\n", LED0_GPIO_LABEL, LED0_GPIO_PIN);
	}

	printk("get led0 pin status = %d\n", gpio_pin_get(led0, LED0_GPIO_PIN));

	return 0;
}
SHELL_CMD_ARG_REGISTER(led0_init_meichi_dev, NULL, LED0_INIT_MEICHI_DEV_CMD_HELP, cmd_led0_init_meichi_dev, 1, 0);

// led0 on or off
typedef enum LED_CTRL {
	LED_OFF = 0,
	LED_ON,
} led_ctrl;
#define LED0_CTRL_MEICHI_DEV_CMD_HELP "Moxa LED0 control operation.\n"\
			"Usage: led0_ctrl_meichi_dev [ctrl]\n"\
			"[ctrl]: 0:OFF, 1:ON\n"
static int cmd_led0_ctrl_meichi_dev(const struct shell *shell, size_t argc, char **argv)
{
	int ret;
	led_ctrl ctrl = LED_ON;
	if (argc < 2) {
		shell_print(shell, LED0_CTRL_MEICHI_DEV_CMD_HELP);
	} else {
		ctrl = (led_ctrl)strtoul(argv[1], NULL, 10);
	}

	switch (ctrl) {
		case LED_ON:
			gpio_pin_set(led0, LED0_GPIO_PIN, ctrl);
			printk("Turn on led0\n");
			break;
		case LED_OFF:
			gpio_pin_set(led0, LED0_GPIO_PIN, ctrl);
			printk("Turn off led0\n");
			break;
		default:
			printk("*** Not execute ***\r\n");
	}

	printk("get led0 pin status = %d\n", gpio_pin_get(led0, LED0_GPIO_PIN));

	return 0;
}
SHELL_CMD_ARG_REGISTER(led0_ctrl_meichi_dev, NULL, LED0_CTRL_MEICHI_DEV_CMD_HELP, cmd_led0_ctrl_meichi_dev, 2, 0);
#endif
```



**Y:\zephyr\bios-moxasrc\modules\board_lib\Kconfig**

```
config LED0_INIT_MEICHI_DEV
	  bool "board_lib MXGPIO shell command support"
	  depends on BOARD_LIB
	  default n
	  help
		This option enables the BOARD_LIB LED_CTRL shell command.
```



**Y:\zephyr\bios-moxasrc\profile\bootloader\prj.conf**

```
# LED
# CONFIG_LED=y
# CONFIG_LED_SHELL=y
CONFIG_BOARD_LIB_MXGPIO_SHELL=y
CONFIG_BOARD_LIB_MXGPIO_THREAD=y
CONFIG_LED0_INIT_MEICHI_DEV=y
```



### Note (Kconfig)

- 如果沒有在`Kconfig`中加入上述程式碼會得到以下輸出

  ![GPIO_error](Ark_practice_images/GPIO_error.jpg)

- Kconfig (configuration system)

  - `Kconfig` build完後的輸出檔為`autoconf.h`
  - 藉由聚集的設定可以跳過使用不到的程式碼來減少code size

  

## Ethernet

TCP和UDP是用來確保傳輸資料的正確性。 [reference link](https://nordvpn.com/zh-tw/blog/tcp-udp-bijiao/)

TCP傳輸資料前，會先將資料分割成較小封包，再將這些封包加上編號，然後送出。接收端收到資料後，TCP會開始檢查資料是否已經全部收到?或是有錯誤或漏掉的?如果都沒有問題，才會將封包依序組合起來。

UDP對接收到的封包不會做任何回應，因此封包可能會在傳輸過程中遺失、重複、或不依照順序送收，抵達速度也可能比接收端的處理速度快。

Socket是應用層與TCP/IP協議通信的中間軟體抽象層，它是一組介面。在設計模式中，把複雜的TCP/IP協議隱藏在Socket介面後面，對用戶來說，一組簡單的介面就是全部，讓Socket去組織數據，以符合指定的協議。[reference link](https://pws.niu.edu.tw/~ttlee/os.101.1/day/socket/)

![](Ark_practice_images/socket-2.jpg)

實際上"建立一個Socket"意味著為一個Socket數據結構分配存儲空間。在使用socket進行網絡傳輸以前，必須配置該socket。sin_family一般為AF_INET，代表Internet（TCP/IP）地址族；sin_port為socket的埠號、sin_addr為IP位址。

- sin_port = 0 系統隨機選擇一個未被使用的埠號
- sin_addr.s_addr = INADDR_ANY 填入本機IP地址



Socket中的bind函數，服務端server透過調用bind函數來配置本地端資訊。Bind函數將socket與本機上的一個埠相連，因此可以在該埠監聽服務請求。 (此部分再描述如何做socket配置)

```c
int bind(int sockfd,struct sockaddr *my_addr, int addrlen);
```

Sockfd是調用socket函數返回的socket描述符，my_addr是一個指向包含有本機IP位址及埠號等資訊的sockaddr_in類型的pointer；addrlen常被設置為sizeof(struct sockaddr_in)。注意在使用bind函數是需要將sin_port和sin_addr轉換成為網絡字節優先順序；而sin_addr不需要轉換。

- htonl()：把32位值從主機字節序轉換成網絡字節序
- **htons()：把16位值從主機字節序轉換成網絡字節序**
- ntohl()：把32位值從網絡字節序轉換成主機字節序
- ntohs()：把16位值從網絡字節序轉換成主機字節序



![](Ark_practice_images/phy-1.jpg)

### Test item

1. eth0 和 eth1 各自做udp loopback測試 (要用mac的方式去做)

   ```c
   struct board_env_ipv4_str *env_ipv4_str = get_env_ipv4_str();
   ```

   取ip string過程: 比對g_binfo.crc與board_info.env結果是否相同 -> 不相同的話由eeprom取值 (先比較crc值，若不同則取用default setting，並存放置eeprom) -> 若以上失敗(沒有外接eeprom)，則取用default setting -> upadate board_env_ipv4_str

2. 在mpos中實作tftp client功能

   透過local array去存取收到的data並print出來
   
   下command:
   
   ```
   tftpc 0x0 192.168.128.180 zephyr.bin 192.168.128.101
   ```

​		zephyr.bin內容: 

​		![](Ark_practice_images/zephyr_bin_content.jpg)

​		result output:

![](Ark_practice_images/tftpc_test_result.jpg)



### Code

**Y:\zephyr_org\bios-moxasrc\modules\board_lib\include\env.h**

board_env_ipv4_str

board_info中包含crc與board_env(socket format)

```c
struct board_env_ipv4_str {
	char *serv_ipv4_addr_str;
	char *eth0_ipv4_addr_str;
	char *eth0_ipv4_mask_str;
	char *eth1_ipv4_addr_str;
	char *eth1_ipv4_mask_str;
};

struct board_info {
	uint16_t crc;
	uint16_t reserve;
	struct board_env env;
};

// g_binfo.env
struct board_env {
	uint32_t serv_ipv4_addr;
	uint32_t eth0_ipv4_addr;
	uint32_t eth0_ipv4_mask;
	uint32_t eth1_ipv4_addr;
	uint32_t eth1_ipv4_mask;
	uint8_t eth0_mac_addr[6];
	uint8_t eth1_mac_addr[6];
	uint32_t dfu_flash_offs;
	uint8_t dfu_flag;
	uint8_t dfu_ec;
};

struct board_info* get_env()
{
	/* Calculate and compare the CRC. */
	if (g_binfo.crc != (uint16_t)crc16_ansi((uint8_t *)&g_binfo.env, sizeof(struct board_env))) {
		if(get_env_eeprom(&g_binfo)) {
			LOG_ERR("get_env_eeprom error!!");
			LOG_INF("get default env");
			if(env_default_no_save(&g_binfo)) {
				LOG_ERR("get_env_eeprom/default env error!!");
				return NULL;
			}
		}
	}
	return &g_binfo;
}

int get_env_eeprom(struct board_info *binfo)
{
	const struct device *eeprom;
	off_t offset = 0U;
	size_t len = sizeof(struct board_info);
	int err;

	eeprom = device_get_binding(ENV_PAGE);
	if (!eeprom) {
		LOG_ERR("EEPROM device not found");
		return -EINVAL;
	}

	err = eeprom_read(eeprom, offset, (uint8_t *)binfo, len);
	if (err) {
		LOG_ERR("EEPROM read failed (err %d)", err);
		return err;
	}

	/* Calculate and compare the CRC. */
	if (binfo->crc != (uint16_t)crc16_ansi((uint8_t *)&binfo->env, sizeof(struct board_env))) {
		LOG_ERR("CRC error!! set env to default.");
		if (env_default(binfo)) {
			return -1;
		}
	}

	update_board_env_ipv4_str(binfo);

	return 0;
}
```



**Y:\zephyr_org\bios-moxasrc\zephyr\include\net\net_ip.h**

socket type [reference link](https://beej-zhtw-gitbook.netdpi.net/02-what-is-socket/2-1-two-internet-sockets)

```c
/** Socket address struct for IPv4. */
struct sockaddr_in {
	sa_family_t			sin_family;    /* AF_INET      */
	uint16_t			sin_port;      /* Port number  */
	struct in_addr		sin_addr;      /* IPv4 address */
};

/** IPv4 address struct */
struct in_addr {
	union {
		uint8_t s4_addr[4];
		uint16_t s4_addr16[2]; 	/* In big endian */
		uint32_t s4_addr32[1]; 	/* In big endian */
		uint32_t s_addr; 		/* In big endian, for POSIX compatibility. */
	};
};

/** Protocol numbers from IANA/BSD */
enum net_ip_protocol {
	IPPROTO_IP = 0,            /**< IP protocol (pseudo-val for setsockopt() */
	IPPROTO_ICMP = 1,          /**< ICMP protocol   */
	IPPROTO_IGMP = 2,          /**< IGMP protocol   */
	IPPROTO_IPIP = 4,          /**< IPIP tunnels    */
	IPPROTO_TCP = 6,           /**< TCP protocol    */
	IPPROTO_UDP = 17,          /**< UDP protocol    */
	IPPROTO_IPV6 = 41,         /**< IPv6 protocol   */
	IPPROTO_ICMPV6 = 58,       /**< ICMPv6 protocol */
	IPPROTO_RAW = 255,         /**< RAW IP packets  */
};

/** Socket type */
enum net_sock_type {
	SOCK_STREAM = 1,           /**< Stream socket type (TCP)*/
	SOCK_DGRAM,                /**< Datagram socket type (UDP)*/
	SOCK_RAW                   /**< RAW socket type      */
};
```



**ip或addr字串與MCU值轉換**

ntop: socket format -> string

pton: string -> socket format

![](Ark_practice_images/socket-1.jpg)



**Y:\zephyr_org\bios-moxasrc\profile\mpos\src\udp_echo.c**

:exclamation: L3 不適合用於loopback測試，因為對於MCU來說都為自己的ip，因此封包根本不會被出送出去

**開UDP server socket流程:**

socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP) -> assign (struct sockaddr_in) bind_addr 的sin_family, sin_port和sin_addr -> bind(serv, (struct sockaddr *)&bind_addr, sizeof(bind_addr)) -> while loop 接收client端

**recvfrom() vs. recv():**

recvfrom多了兩個參數，可以用來接收對端的地址，對於udp這種無線連接，可以很方便地進行回復。如果在udp中使用recv，那麼就不知道该回覆给誰，但如果不需要回覆的話也可以使用。對於tcp是已經知道對端的，界不需要每次接收多收一位址。

The recvfrom() function shall receive a message from a connection-mode or connectionless-mode socket. It is normally used with connectionless-mode sockets because it permits the application to retrieve the source address of received data.

```c
struct k_thread udpser_thread;
#define UDPSER_STACKSIZE 4096
K_THREAD_STACK_DEFINE(udpser_stack, UDPSER_STACKSIZE);
/* Global mutex to protect critical resources. */
K_MUTEX_DEFINE(udpser_thread_lock);

//建立udp server
//server port = 4242
#define UDPSER_CMD_HELP "UDP echo server.\n"    \
			"Usage: udpser [local_ip]"
static int cmd_udpser(const struct shell *shell, size_t argc, char **argv)
{
	k_thread_create(&udpser_thread, udpser_stack, UDPSER_STACKSIZE,
			(k_thread_entry_t) udpser_thread_entry,
			(void *)&argc, (void *)argv, NULL, K_PRIO_COOP(7), 0, K_NO_WAIT);
	k_thread_name_set(&udpser_thread, "UDP_Server");
	return 0;
}
SHELL_CMD_ARG_REGISTER(udpser, NULL, UDPSER_CMD_HELP, cmd_udpser, 1, 1);

// thread實作
void udpser_thread_entry(void *p1, void *p2, void *p3)
{
	size_t *argc = (size_t *)p1;
	char  **argv =  (char **)p2;
	int serv;
	struct sockaddr_in bind_addr;
	struct board_env_ipv4_str *env_ipv4_str = get_env_ipv4_str();
	const char *def_loc_addr = env_ipv4_str->eth0_ipv4_addr_str;

	if ((*argc >= 2) && (argv[1][0] != '-')) {
		def_loc_addr = argv[1];
	}

	/* Obtain Global Lock before accessing critical resources. */
	k_mutex_lock(&udpser_thread_lock, K_FOREVER);

	serv = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (serv < 0) {
		LOG_ERR("error: socket: %d\n", errno);
		close(serv);
		return;
	}

	bind_addr.sin_family = AF_INET;
	bind_addr.sin_port = htons(BIND_PORT);
	// bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (inet_pton(AF_INET, def_loc_addr, &bind_addr.sin_addr) <= 0) {
		LOG_ERR("error: inet_pton %s: %d\n", def_loc_addr, errno);
		close(serv);
		return;
	}

	if (bind(serv, (struct sockaddr *)&bind_addr, sizeof(bind_addr)) < 0) {
		LOG_ERR("error: bind: %d\n", errno);
		close(serv);
		return;
	}

	LOG_DBG("Single-threaded UDP echo server waits for a connection on port %d...\n", BIND_PORT);

	while (1) {
		struct sockaddr_in client_addr;
		socklen_t client_addr_len = sizeof(client_addr);
		char addr_str[32];

		while (1) {
			char buf[ETH_BUFF_SIZE], *p;
			int len = recvfrom(serv, buf, sizeof(buf), 0, (struct sockaddr*)&client_addr, &client_addr_len);
			int out_len;

			if (len <= 0) {
				if (len < 0) {
					LOG_ERR("error: recv: %d\n", errno);
				}
				break;
			}

			p = buf;
			do {
				out_len = sendto(serv, p, len, 0, (struct sockaddr*)&client_addr, client_addr_len);
				if (out_len < 0) {
					LOG_ERR("error: send: %d\n", errno);
					goto error;
				}
				p += out_len;
				len -= out_len;
			} while (len);
		}

error:
		inet_ntop(client_addr.sin_family, &client_addr.sin_addr, addr_str, sizeof(addr_str));
		LOG_DBG("Connection from %s closed\n", addr_str);
	}

	close(serv);
	/* Clean up. */
	k_mutex_unlock(&udpser_thread_lock);
}

//建立udp client
//client port = 0
//udp_client(eth1_addr_str,eth1_addr_str,10,5000)
//send 10次64B的data
int udp_client(const char *ser_addr, const char *loc_addr, int count, uint32_t timeout)
{
	int ret = 0;
	int sockfd = -1;
	sockfd = udp_client_init(loc_addr);
	if (sockfd < 0) {
		return -1;
	}
	ret = udp_client_test(sockfd, ser_addr, count, timeout);
	close(sockfd);
	return ret;
}

//傳送data與比較send_data_buf和recv_data_buf
int udp_client_test(int sockfd, const char *ser_addr, int count, uint32_t timeout)
{
	uint8_t send_data_buf[ETH_BUFF_SIZE];
	uint8_t recv_data_buf[ETH_BUFF_SIZE];
	int len = 0;
	int cnt = count;
	struct sockaddr_in servaddr;
	socklen_t servaddr_len = sizeof(servaddr);

	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(BIND_PORT);
	if (inet_pton(AF_INET, ser_addr, &servaddr.sin_addr) <= 0) {
		LOG_ERR("error: inet_pton %s: %d\n", ser_addr, errno);
		return -1;
	}

	/* Setup FDS. */
	int stat;
	struct pollfd fds;
	fds.fd = sockfd;
	fds.events = ZSOCK_POLLIN;
	while ((cnt < 0) ? true : cnt--) {

		for (int i = 0; i < ETH_BUFF_SIZE; i++) {
			send_data_buf[i] = (uint8_t)i;
		}

		len = sendto(sockfd, send_data_buf, sizeof(send_data_buf), 0, (struct sockaddr *)&servaddr, servaddr_len);
		if (len < 0) {
			LOG_ERR("error: send: %s(errno: %d)\n", strerror(errno), errno);
			return -1;
		}

		memset(recv_data_buf, 0, sizeof(recv_data_buf));
		//poll函數用來監看有沒有任何事件發生，無論IO是否ready，都會返回數值。執行结果：為0表示超時前没有任何事件發生；-1表示失敗；成功則返回結構中revents步為0的文件描述符個數
		stat = poll(&fds, 1, timeout); //ms
		if (stat > 0) {
			len = recvfrom(sockfd, recv_data_buf, sizeof(recv_data_buf), 0, (struct sockaddr *)&servaddr, &servaddr_len);
			if (len < 0) {
				LOG_ERR("error: recv: %s(errno: %d)\n", strerror(errno), errno);
				return -1;
			}

			if (memcmp(recv_data_buf, send_data_buf, sizeof(recv_data_buf)) != 0) {
				LOG_ERR("Data does not match.\n");
				return -1;
			}
		} else if (stat == 0) {
			LOG_ERR("Socket poll timeout.");
			return -1;
		} else {
			LOG_ERR("Socket poll Error. error code: %d", stat);
			return -1;
		}
	}

	return 0;
}

```



**Y:\zephyr_org\bios-moxasrc\modules\board_lib\src\net.c**

tftp client功能

```c
struct k_thread tftpc_thread;
K_THREAD_STACK_DEFINE(tftpc_stack, TFTPC_STACKSIZE);

/* Global mutex to protect critical resources. */
K_MUTEX_DEFINE(tftpc_thread_lock);

//建立tftpc thread
static int cmd_tftpc(const struct shell *shell, size_t argc, char **argv)
{
	k_thread_create(&tftpc_thread, tftpc_stack, TFTPC_STACKSIZE,
			(k_thread_entry_t) tftpc_get_thread_entry,
			(void *)&argc, (void *)argv, NULL, K_PRIO_COOP(7), 0, K_NO_WAIT);
	k_thread_name_set(&tftpc_thread, "TFTP Client");
	return 0;
}
SHELL_CMD_ARG_REGISTER(tftpc, NULL, TFTPC_CMD_HELP, cmd_tftpc, 1, 4);

//實作tftp client功能
#define TFTPC_CMD_HELP "TFTP client get function.\n"    \
			"Usage: tftpc [mem] [server_ip] [filename] [local_ip]"
void tftpc_get_thread_entry(void *p1, void *p2, void *p3)
{
	size_t *argc = (size_t *)p1;
	char **argv = (char **)p2;
	int res = 0;
	struct sockaddr_in server_addr;
	struct tftpc client;
	const char *filename = "zephyr.bin";
	const char *mode = "octet";
	//uint32_t user_buf_val = TFTP_BUFF_ADDR; //0x80000000
	//uint32_t *user_buf = (uint32_t *) user_buf_val;

	// meichi test
	uint8_t user_buf_val[20];
	memset(&user_buf_val[0], 0, sizeof(user_buf_val));
	uint8_t *user_buf = &user_buf_val[0];

	struct board_env_ipv4_str *env_ipv4_str = get_env_ipv4_str();
	const char *def_ser_addr = env_ipv4_str->serv_ipv4_addr_str;
	const char *def_loc_addr = env_ipv4_str->eth0_ipv4_addr_str;

	/* Obtain Global Lock before accessing critical resources. */
	k_mutex_lock(&tftpc_thread_lock, K_FOREVER);

	if ((*argc >= 2) && (argv[1][0] != '-')) {
		//user_buf_val = (uint32_t)strtoul(argv[1], NULL, 16);
		//user_buf = (uint32_t *) user_buf_val;
	}
	if ((*argc >= 3) && (argv[2][0] != '-')) {
		def_ser_addr = argv[2];
	}
	if ((*argc >= 4) && (argv[3][0] != '-')) {
		filename = argv[3];
	}
	if ((*argc >= 5) && (argv[4][0] != '-')) {
		def_loc_addr = argv[4];
	}
	LOG_INF("mem: %p, server_ip: %s, filename: %s, local_ip: %s", user_buf, def_ser_addr, filename, def_loc_addr);

	client.user_buf = (uint8_t *) user_buf; //BL存放起始位址
	//client.user_buf_size = TFTP_BUFF_SIZE;
	client.user_buf_size = sizeof(user_buf_val);

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(TFTP_PORT);
	inet_pton(AF_INET, def_ser_addr, &server_addr.sin_addr);

	//res = tftp_get((struct sockaddr *)&server_addr, &client, filename, mode, def_loc_addr);
	res = tftp_get((struct sockaddr *)&server_addr, &client, filename, mode);
	if (res)
		LOG_ERR("[%s] tftp get error: %d", __func__, res);
	else
		LOG_DBG("[%s] tftp get PASS \n", __func__);
		//LOG_INF("received data: %d %d %d %d %d", *user_buf, *(user_buf+1), *(user_buf+2), *(user_buf+3), *(user_buf+4));
		LOG_INF("received data: %d %d %d %d %d", *user_buf, *user_buf++, *user_buf++, *user_buf++, *user_buf++);
		
	/* Clean up. */
	k_mutex_unlock(&tftpc_thread_lock);
}
```



### Note

#### Tftp server 設定

![](Ark_practice_images/tftpserver_setting.jpg)

![](Ark_practice_images/tftpc_test_wireshark_result.jpg)



zephyr會規劃一個區間給socket，如果拿掉init status flag且重複執行create socket會出現RAM相關error

```c
	//init send & recv socket
	//if (!g_packet_init_status) {
		ret = packet_socket_init(&g_packet);
		if(ret) {
			LOG_DBG("packet_socket_init FAIL");
			return ret;
		}
	//	g_packet_init_status = 1;
	//}
```

Error result:

![](Ark_practice_images/socket_error_result.jpg)
